# Someutils

Various utilities for Python projects.

## Paths
Require _pathlib_.

#### remove_empty_directories(path)
Recursively remove the empty directories within a path.
#### is_existing directory(path)
Check is a path is an existing directory.


## Images
Require _pathlib_, _PIL_ and _exifread_.

#### is_image(path)
Check if a path is a valid image file.
#### get_tag(path, tag)
Get a specific tag from a path.

 