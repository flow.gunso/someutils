# Changelog

## 0.4.3 - 2018/10/09
- In **images.is_image()**, raise an _exception_ instead of returning _False_
- Use **setup.py** for dependencies instead of **requirements.txt**
#### hotfix 1 - 2018/10/09
- Replace alpine with slim (Debian Stretch) in **gitlab-ci.yml**

## 0.4.2 - 2018/09/03
- I should have been cautious: see [PyPi file name reuse](https://pypi.org/help/#file-name-reuse)

## 0.4.1 - 2018/09/03
- Fix __gitlab-ci.yml__:
  - Improve before_script to use a local .pypirc configuration file
  - Remove tag validation
  - Remove environment variables
  - Enable PyPi test servers
- Fix __setup.py__ url

## 0.4 - 2018/09/03
- Update one __images.py__ docstrings
- Match all exceptions in __images.is_image(path)__
- Fix __requirements.txt__ by adding _Pillow_ and _pathlib_
- Licence the package to Mozilla Public Licence v2:
  - Add licence headers to source code files
  - Add a LICENCE file
- Prepare for upcoming upload to PyPi:
    - Improve __setup.py__ using PyPA User Guide to packaging
    - Enable GitLab CI/CD for PyPi deployments
- Improve __README.md__ readabilty by replacing lists with headers.

## 0.3.1 - 2018/05/27
- Add some requirements packages

## 0.3 - 2018/05/27
- Fix images.get_tag() path opening
- Restructured as package

## 0.2.5 - 2018/05/11
- Fix images.get_tag() gets a path not an open file anymore

## 0.2 - 2018/05/11
- Initial commit